# GDAL Docker image supporting oracle formats

Unlike Windows installations, when installing GDAL/OGR on a Linux machine the *Oracle OCI* (vector) and *Oracle GeoRaster* (raster) formats are not supported by default.

To remedy this, it is necessary to compile gdal yourself and to follow certain precautions.

We will build a docker image that meets these criteria.


## Environment variables

During the build, some environment variables needs to be setted:

- **ORACLE_HOME:** pointing to the directory where the Oracle software is installed. More specifically, a valid Oracle Instant Client.

- **LD_LIBRARY_PATH:**  search path environment variable for the linux shared library. It contains directories where libraries are searched for first before the standard set of directories.

- **PATH:** to tells the shell which directories to search for executable files. In our case, we need to add $ORACLE_HOME/bin to the list.

## Docker image
In this case, a docker image is built from an Ubuntu 20.04 image.

## Oracle instant client64

The necessary Oracle resources are not added to this directory for licensing reasons.
You can find them at: https://www.oracle.com/database/technologies/instant-client.html
and add them in the same folder than the dockerfile.

In our case, we work with:
- oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
- oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm

Those 2 Red Hat packages (.rpm) will be converted to Debian packages (.deb) during the build.

**Important note: I choose not to work with 19 instantclient because I met some issues the first time I tried.
In addition, this version supports up to the most recent versions of Oracle databases, from 11.2 to 19c.**

## GDAL source code
GDAL source code is cloned from the official Github repository.

https://github.com/OSGeo/gdal.git

We are using a specific version number wich is passed as parameter in the dockerfile.
It corresponds to the GIT branch name we will use.

The dependencies are taken from the Ubuntu QGIS installation.
I need to clean it and delete unneeded dependencies.


## Run it


If the Oracle Instant client packages are in the same folder, we can build the image:
```
docker build -t ubuntu-gdal-oracle -f Dockerfile-12.2 .
```
Then, we can run an instance:
```
docker run -it \
  -p 7777:8888 \
  -p 22222:22  \
  --name ubuntu-gdal-auto ubuntu-gdal-oracle:latest
```
I choose to expose the port 8888 for the Jupyter environment and the port 22 to access using SSH remotely.


## Test Oracle format support

To test the Oracle Spatial (vector) format support, you can run the following command in the container:

### Oracle Spatial (OCI) support

```
ogrinfo --formats | grep OCI
```

And you should get the following result:

```
OCI -vector- (rw+): | Oracle Spatial
```

### Oracle Spatial Georaster support

For the Oracle GeoRaster support, it is the same logic:

```
gdalinfo --formats | grep GeoRaster
```
Which should give you back:
```
GeoRaster -raster- (rw+s): Oracle Spatial GeoRaster
```
