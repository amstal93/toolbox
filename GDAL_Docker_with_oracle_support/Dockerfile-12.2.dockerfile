# docker build -t ubuntu-gdal-oracle .
# docker run -it --name ubuntu-gdal-auto ubuntu-gdal-oracle:latest

ARG BASE_IMAGE=ubuntu:20.04
FROM $BASE_IMAGE

ARG GDAL_VERSION=3.0
RUN apt-get update

WORKDIR /root
RUN mkdir oracle
WORKDIR oracle

COPY oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm .
COPY oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm .

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --fix-missing --no-install-recommends alien
RUN alien oracle-instantclient12.2-devel-12.2.0.1.0-1.x86_64.rpm
RUN alien oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm
RUN dpkg -i oracle-instantclient12.2-devel_12.2.0.1.0-2_amd64.deb
RUN dpkg -i oracle-instantclient12.2-basic_12.2.0.1.0-2_amd64.deb

ENV ORACLE_HOME=/usr/lib/oracle/12.2/client64
ENV LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib:/usr/local/lib
ENV PATH=$PATH:$ORACLE_HOME/bin

RUN mkdir $ORACLE_HOME/sdk
RUN ln -s /usr/include/oracle/12.2/client64 $ORACLE_HOME/sdk/include
RUN ln -s /usr/include/oracle/12.2/client64 $ORACLE_HOME/include

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --fix-missing --no-install-recommends software-properties-common
RUN add-apt-repository ppa:ubuntugis/ubuntugis-unstable
RUN apt-get update

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --fix-missing --no-install-recommends bison ca-certificates ccache cmake cmake-curses-gui dh-python doxygen expect flex flip gdal-bin git graphviz grass-dev libexiv2-dev libexpat1-dev libfcgi-dev libgdal-dev libgeos-dev libgsl-dev libpq-dev libproj-dev libprotobuf-dev libqca-qt5-2-dev libqca-qt5-2-plugins libqscintilla2-qt5-dev libqt5opengl5-dev libqt5serialport5-dev libqt5sql5-sqlite libqt5svg5-dev libqt5webkit5-dev libqt5xmlpatterns5-dev libqwt-qt5-dev libspatialindex-dev libspatialite-dev libsqlite3-dev libsqlite3-mod-spatialite libyaml-tiny-perl libzip-dev lighttpd locales ninja-build ocl-icd-opencl-dev opencl-headers pkg-config poppler-utils protobuf-compiler pyqt5-dev pyqt5-dev-tools pyqt5.qsci-dev python3-all-dev python3-autopep8 python3-dateutil python3-dev python3-future python3-gdal python3-httplib2 python3-jinja2 python3-lxml python3-markupsafe python3-mock python3-nose2 python3-owslib python3-plotly python3-psycopg2 python3-pygments python3-pyproj python3-pyqt5 python3-pyqt5.qsci python3-pyqt5.qtsql python3-pyqt5.qtsvg python3-pyqt5.qtwebkit python3-requests python3-sip python3-sip-dev python3-six python3-termcolor python3-tz python3-yaml qt3d-assimpsceneimport-plugin qt3d-defaultgeometryloader-plugin qt3d-gltfsceneio-plugin qt3d-scene2d-plugin qt3d5-dev qt5-default qt5keychain-dev qtbase5-dev qtbase5-private-dev qtpositioning5-dev qttools5-dev qttools5-dev-tools saga spawn-fcgi pandoc xauth xfonts-100dpi xfonts-75dpi xfonts-base xfonts-scalable xvfb

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --fix-missing --no-install-recommends g++
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --fix-missing --no-install-recommends libaio1

WORKDIR ..
RUN git clone https://github.com/OSGeo/gdal.git
WORKDIR gdal
RUN git branch -a
RUN git checkout "release/$GDAL_VERSION"

WORKDIR gdal

RUN ./configure --with-libtiff=internal --with-geotiff=internal --with-jpeg=internal --with-gif=internal --with-python=/usr/bin/python3 --with-openjpeg=yes --with-oci=yes --with-oci-include=/usr/lib/oracle/12.2/client64/include --with-oci-lib=/usr/lib/oracle/12.2/client64/lib LDFLAGS="-L/usr/lib/oracle/12.2/client64/lib" LD_SHADE="gcc -shared"

RUN make
RUN make install

#ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

WORKDIR /usr/local/bin/

CMD /bin/bash
