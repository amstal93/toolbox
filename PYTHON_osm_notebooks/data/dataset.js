var dataset = {
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "properties": {
        "NAME": "Jean",
        "ADDRESS_FULL": "Rue Pouplin 10. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.5626333,
          50.640728
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Cendrillon",
        "ADDRESS_FULL": "Rue du Pot d\u2019or. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.5694161,
          50.6415021
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Linette",
        "ADDRESS_FULL": "Rue cath\u00e9drale 46. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          NaN,
          NaN
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Marc",
        "ADDRESS_FULL": "Boulevard d\u2019Avroy 63. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          NaN,
          NaN
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Arlette",
        "ADDRESS_FULL": "Rue Saint L\u00e9onard, 50. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.586755904941956,
          50.648103250000005
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Cl\u00e9mence",
        "ADDRESS_FULL": "F\u00e9ronstr\u00e9e, 17. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.57762553852254,
          50.646152
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Martine",
        "ADDRESS_FULL": "Rue hors ch\u00e2teau, 25. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.577115,
          50.6467526
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Mireille",
        "ADDRESS_FULL": "Boulevard de l\u2019Automobile, 1. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.589853541075725,
          50.62598775
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Anais",
        "ADDRESS_FULL": "Quai des Vennes,8. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.589554,
          50.6184023
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Louis",
        "ADDRESS_FULL": "Boulevard du rectorat, 2. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.5991346,
          50.5762785
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Julie",
        "ADDRESS_FULL": "Rue Saint Gilles 200. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.561749401100122,
          50.6376296
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Pauline",
        "ADDRESS_FULL": "Rue des \u00e9coles 18. 4000 Li\u00e8ge. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.580064003515437,
          50.61364475
        ]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "NAME": "Simon",
        "ADDRESS_FULL": "Rue du Pont, 7. 4100 Seraing. Belgique"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [
          5.507605324723716,
          50.61744085
        ]
      }
    }
  ]
};